﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NicroWare.Lib.MatLabGen
{
    public enum ShapeType
    {
        Place,
        Transition,
        Unknown,
    }

    public class ShapeInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ShapeType Geometry { get; set; }
        public string Text { get; set; }
    }

    public class Connection
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public int Count { get; set; } = 1;
    }

    public class Petrinet
    {
        public Dictionary<int, ShapeInfo> Shapes { get; } = new Dictionary<int, ShapeInfo>();
        public Dictionary<int, Connection> Connections { get; } = new Dictionary<int, Connection>();

        public List<ShapeInfo> Places { get; } = new List<ShapeInfo>();
        public List<ShapeInfo> Transitions { get; } = new List<ShapeInfo>();

        public void AddShape(ShapeInfo info)
        {
            Shapes[info.ID] = info;
            switch (info.Geometry)
            {
                case ShapeType.Place:
                    Places.Add(info);
                    break;
                case ShapeType.Transition:
                    Transitions.Add(info);
                    break;
                default:
                    Console.WriteLine("Trying to add an unknown Geometry");
                    break;
            }
        }

        public void AddConnection(Connection connection)
        {
            Connections[connection.ID] = connection;
        }

        public string[] VerifyPetrinet()
        {
            Dictionary<string, bool> names = new Dictionary<string, bool>();
            List<string> errors = new List<string>();
            Dictionary<ShapeInfo, int> CountConn = new Dictionary<ShapeInfo, int>();
            foreach (var v in Places)
            {
                CountConn[v] = 0;
                if (string.IsNullOrWhiteSpace(v.Text))
                {
                    errors.Add($"Place found without name");
                    continue;
                }
                else if (names.ContainsKey(v.Text))
                {
                    errors.Add($"Duplicate key found: {v.Text}");
                }
                names[v.Text] = true;
                if (!v.Text.StartsWith("p"))
                {
                    errors.Add($"Place not starting with an p: {v.Text}");
                }
            }
            foreach (var v in Transitions)
            {
                CountConn[v] = 0;
                if (string.IsNullOrWhiteSpace(v.Text))
                {
                    errors.Add($"Transition found without name");
                    continue;
                }
                else if (names.ContainsKey(v.Text))
                {
                    errors.Add($"Duplicate key found: {v.Text}");
                    continue;
                }
                names[v.Text] = true;
                if (!v.Text.StartsWith("t"))
                {
                    errors.Add($"Transition not starting with an t: {v.Text}");
                }
            }
            foreach (var conn in Connections)
            {
                if (conn.Value.StartIndex == 0 && conn.Value.EndIndex == 0)
                {
                    errors.Add($"Found non connected connection");
                    continue;
                }
                if (conn.Value.StartIndex == 0)
                {
                    var shape = Shapes[conn.Value.EndIndex];
                    CountConn[shape]++;
                    errors.Add($"Connection connected to {shape.Text} is missing a start");
                    continue;
                }
                if (conn.Value.EndIndex == 0)
                {
                    var shape = Shapes[conn.Value.StartIndex];
                    CountConn[shape]++;
                    errors.Add($"Connection connected to {shape.Text} is missing a end");
                    continue;
                }

                CountConn[Shapes[conn.Value.StartIndex]]++;
                CountConn[Shapes[conn.Value.EndIndex]]++;

                if (Shapes[conn.Value.StartIndex].Geometry == Shapes[conn.Value.EndIndex].Geometry)
                {
                    errors.Add($"Connection connected between the same geometry: {Shapes[conn.Value.StartIndex].Text} -> {Shapes[conn.Value.EndIndex].Text}");
                    continue;
                }
            }
            foreach (var pair in CountConn)
            {
                if (pair.Value == 0)
                {
                    errors.Add($"Found {pair.Key.Geometry} with no connections: {pair.Key.Text}");
                }
            }
            return errors.ToArray();
        }


        public void GeneratePetrinet(string fileName, string modelName)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"function [png] = {modelName}_pdf()");
            builder.AppendLine($"png.PN_name = '{modelName}';");
            builder.AppendLine($"png.set_of_Ps = {{{string.Join(", ...\n\t\t\t\t", Places.Select(x => $"'{x.Text}'").ToList().Action(x => x.Sort()))}}};");
            builder.AppendLine($"png.set_of_Ts = {{{string.Join(", ...\n\t\t\t\t", Transitions.Select(x => $"'{x.Text}'").ToList().Action(x => x.Sort()))}}};");
            builder.AppendLine($"png.set_of_As = {{{string.Join(", ...\n\t\t\t\t", Connections.Select(x => $"'{Shapes[x.Value.StartIndex].Text}', '{Shapes[x.Value.EndIndex].Text}', {x.Value.Count}"))}}};");

            File.WriteAllText(fileName, builder.ToString());
        }
    }

    public static class Extension
    {
        public static T Action<T>(this T t, Action<T> action)
        {
            action(t);
            return t;
        }
    }

}
