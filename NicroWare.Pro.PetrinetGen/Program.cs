﻿using NicroWare.Lib.MatLabGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NicroWare.Pro.PetrinetGen
{
    class Program
    {
        static void Main(string[] args)
        {
            // string modelName = "loadbalance";
            string modelName = "testmodel";
            // string fileName = $@"C:\Users\Nicolas Fløysvik\Desktop\{modelName}_pdf.m";
            string fileName = $@"..\..\..\{modelName}_pdf.m";
            Console.WriteLine("Hello World!");

            // sld sld = ReadSLDFrom(@"C:\Users\Nicolas Fløysvik\Desktop\pterinetdef.pptx");
            // sld sld = ReadSLDFrom(@"..\..\..\pterinetdef.pptx", 1);
            sld sld = ReadSLDFrom(@"..\..\..\test.pptx", 1);


            Petrinet net = GenerateFromPPT(sld);
            
            net.GeneratePetrinet(fileName, modelName);

            Console.Read();
        }

        private static Petrinet GenerateFromPPT(sld sld)
        {
            Petrinet net = new Petrinet();

            foreach (object o in sld.cSld.spTree.Items)
            {
                if (o is sldCSldSpTreeSP shape)
                {
                    ShapeInfo si = shape.GetInfo();
                    net.AddShape(si);
                    Console.WriteLine($"SHAPE: {si.ID} \"{si.Name}\" {si.Geometry} {si.Text}");
                }
                else if (o is sldCSldSpTreeCxnSp connection)
                {
                    Connection conn = connection.GetInfo();
                    net.AddConnection(conn);
                    // Connections[conn.ID] = conn;
                    Console.WriteLine($"LINE:  {conn.ID} \"{conn.Name}\" {conn.StartIndex} -> {conn.EndIndex}");
                }
            }
            return net;
        }
       
        static sld ReadSLDFrom(string path, int slideNr)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(sld));
            using (ZipArchive file = ZipFile.OpenRead(path))
            {
                ZipArchiveEntry entry = file.GetEntry($"ppt/slides/slide{slideNr}.xml");
                using (Stream s = entry.Open())
                {
                    sld sld = (sld)serializer.Deserialize(s);
                    return sld;
                }
            }
        }
    }

    public static class ShapeExtension
    {
        public static ShapeType GetShapeType(string shape)
        {
            switch (shape)
            {
                case "rect":
                    return ShapeType.Transition;
                case "ellipse":
                    return ShapeType.Place;
                default:
                    return ShapeType.Unknown;
            }
        }

        public static ShapeInfo GetInfo(this sldCSldSpTreeSP pptShape)
        {
            return new ShapeInfo()
            {
                ID = pptShape.nvSpPr.cNvPr.id,
                Name = pptShape.nvSpPr.cNvPr.name,
                Geometry = GetShapeType(pptShape.spPr.prstGeom.prst),
                Text = pptShape.txBody.p.r.t,
            };
        }

        public static Connection GetInfo(this sldCSldSpTreeCxnSp pptLine)
        {
            return new Connection()
            {
                ID = pptLine.nvCxnSpPr.cNvPr.id,
                Name = pptLine.nvCxnSpPr.cNvPr.name,
                StartIndex = pptLine.nvCxnSpPr.cNvCxnSpPr.stCxn.id,
                EndIndex = pptLine.nvCxnSpPr.cNvCxnSpPr.endCxn.id,
            };
        }
    }
}