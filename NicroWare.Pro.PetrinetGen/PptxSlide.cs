﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NicroWare.Pro.PetrinetGen
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main", IsNullable = false)]
    public partial class sld
    {

        private sldCSld cSldField;

        private sldClrMapOvr clrMapOvrField;

        /// <remarks/>
        public sldCSld cSld
        {
            get
            {
                return this.cSldField;
            }
            set
            {
                this.cSldField = value;
            }
        }

        /// <remarks/>
        public sldClrMapOvr clrMapOvr
        {
            get
            {
                return this.clrMapOvrField;
            }
            set
            {
                this.clrMapOvrField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSld
    {

        private sldCSldSpTree spTreeField;

        private sldCSldExtLst extLstField;

        /// <remarks/>
        public sldCSldSpTree spTree
        {
            get
            {
                return this.spTreeField;
            }
            set
            {
                this.spTreeField = value;
            }
        }

        /// <remarks/>
        public sldCSldExtLst extLst
        {
            get
            {
                return this.extLstField;
            }
            set
            {
                this.extLstField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTree
    {

        private object[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("cxnSp", typeof(sldCSldSpTreeCxnSp))]
        [System.Xml.Serialization.XmlElementAttribute("grpSpPr", typeof(sldCSldSpTreeGrpSpPr))]
        [System.Xml.Serialization.XmlElementAttribute("nvGrpSpPr", typeof(sldCSldSpTreeNvGrpSpPr))]
        [System.Xml.Serialization.XmlElementAttribute("sp", typeof(sldCSldSpTreeSP))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeCxnSp
    {

        private sldCSldSpTreeCxnSpNvCxnSpPr nvCxnSpPrField;

        private sldCSldSpTreeCxnSpSpPr spPrField;

        private sldCSldSpTreeCxnSpStyle styleField;

        /// <remarks/>
        public sldCSldSpTreeCxnSpNvCxnSpPr nvCxnSpPr
        {
            get
            {
                return this.nvCxnSpPrField;
            }
            set
            {
                this.nvCxnSpPrField = value;
            }
        }

        /// <remarks/>
        public sldCSldSpTreeCxnSpSpPr spPr
        {
            get
            {
                return this.spPrField;
            }
            set
            {
                this.spPrField = value;
            }
        }

        /// <remarks/>
        public sldCSldSpTreeCxnSpStyle style
        {
            get
            {
                return this.styleField;
            }
            set
            {
                this.styleField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeCxnSpNvCxnSpPr
    {

        private sldCSldSpTreeCxnSpNvCxnSpPrCNvPr cNvPrField;

        private sldCSldSpTreeCxnSpNvCxnSpPrCNvCxnSpPr cNvCxnSpPrField;

        private object nvPrField;

        /// <remarks/>
        public sldCSldSpTreeCxnSpNvCxnSpPrCNvPr cNvPr
        {
            get
            {
                return this.cNvPrField;
            }
            set
            {
                this.cNvPrField = value;
            }
        }

        /// <remarks/>
        public sldCSldSpTreeCxnSpNvCxnSpPrCNvCxnSpPr cNvCxnSpPr
        {
            get
            {
                return this.cNvCxnSpPrField;
            }
            set
            {
                this.cNvCxnSpPrField = value;
            }
        }

        /// <remarks/>
        public object nvPr
        {
            get
            {
                return this.nvPrField;
            }
            set
            {
                this.nvPrField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeCxnSpNvCxnSpPrCNvPr
    {

        private extLst extLstField;

        private byte idField;

        private string nameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public extLst extLst
        {
            get
            {
                return this.extLstField;
            }
            set
            {
                this.extLstField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class extLst
    {

        private extLstExt extField;

        /// <remarks/>
        public extLstExt ext
        {
            get
            {
                return this.extField;
            }
            set
            {
                this.extField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class extLstExt
    {

        private creationId creationIdField;

        private string uriField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.microsoft.com/office/drawing/2014/main")]
        public creationId creationId
        {
            get
            {
                return this.creationIdField;
            }
            set
            {
                this.creationIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uri
        {
            get
            {
                return this.uriField;
            }
            set
            {
                this.uriField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.microsoft.com/office/drawing/2014/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.microsoft.com/office/drawing/2014/main", IsNullable = false)]
    public partial class creationId
    {

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeCxnSpNvCxnSpPrCNvCxnSpPr
    {

        private object cxnSpLocksField;

        private stCxn stCxnField;

        private endCxn endCxnField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public object cxnSpLocks
        {
            get
            {
                return this.cxnSpLocksField;
            }
            set
            {
                this.cxnSpLocksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public stCxn stCxn
        {
            get
            {
                return this.stCxnField;
            }
            set
            {
                this.stCxnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public endCxn endCxn
        {
            get
            {
                return this.endCxnField;
            }
            set
            {
                this.endCxnField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class stCxn
    {

        private byte idField;

        private byte idxField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte idx
        {
            get
            {
                return this.idxField;
            }
            set
            {
                this.idxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class endCxn
    {

        private byte idField;

        private byte idxField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte idx
        {
            get
            {
                return this.idxField;
            }
            set
            {
                this.idxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeCxnSpSpPr
    {

        private xfrm xfrmField;

        private prstGeom prstGeomField;

        private ln lnField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public xfrm xfrm
        {
            get
            {
                return this.xfrmField;
            }
            set
            {
                this.xfrmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public prstGeom prstGeom
        {
            get
            {
                return this.prstGeomField;
            }
            set
            {
                this.prstGeomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public ln ln
        {
            get
            {
                return this.lnField;
            }
            set
            {
                this.lnField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class xfrm
    {

        private xfrmOff offField;

        private xfrmExt extField;

        private xfrmChOff chOffField;

        private xfrmChExt chExtField;

        private int rotField;

        private bool rotFieldSpecified;

        private byte flipHField;

        private bool flipHFieldSpecified;

        private byte flipVField;

        private bool flipVFieldSpecified;

        /// <remarks/>
        public xfrmOff off
        {
            get
            {
                return this.offField;
            }
            set
            {
                this.offField = value;
            }
        }

        /// <remarks/>
        public xfrmExt ext
        {
            get
            {
                return this.extField;
            }
            set
            {
                this.extField = value;
            }
        }

        /// <remarks/>
        public xfrmChOff chOff
        {
            get
            {
                return this.chOffField;
            }
            set
            {
                this.chOffField = value;
            }
        }

        /// <remarks/>
        public xfrmChExt chExt
        {
            get
            {
                return this.chExtField;
            }
            set
            {
                this.chExtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int rot
        {
            get
            {
                return this.rotField;
            }
            set
            {
                this.rotField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool rotSpecified
        {
            get
            {
                return this.rotFieldSpecified;
            }
            set
            {
                this.rotFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte flipH
        {
            get
            {
                return this.flipHField;
            }
            set
            {
                this.flipHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool flipHSpecified
        {
            get
            {
                return this.flipHFieldSpecified;
            }
            set
            {
                this.flipHFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte flipV
        {
            get
            {
                return this.flipVField;
            }
            set
            {
                this.flipVField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool flipVSpecified
        {
            get
            {
                return this.flipVFieldSpecified;
            }
            set
            {
                this.flipVFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class xfrmOff
    {

        private int xField;

        private int yField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int x
        {
            get
            {
                return this.xField;
            }
            set
            {
                this.xField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int y
        {
            get
            {
                return this.yField;
            }
            set
            {
                this.yField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class xfrmExt
    {

        private int cxField;

        private int cyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int cx
        {
            get
            {
                return this.cxField;
            }
            set
            {
                this.cxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int cy
        {
            get
            {
                return this.cyField;
            }
            set
            {
                this.cyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class xfrmChOff
    {

        private byte xField;

        private byte yField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte x
        {
            get
            {
                return this.xField;
            }
            set
            {
                this.xField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte y
        {
            get
            {
                return this.yField;
            }
            set
            {
                this.yField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class xfrmChExt
    {

        private byte cxField;

        private byte cyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte cx
        {
            get
            {
                return this.cxField;
            }
            set
            {
                this.cxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte cy
        {
            get
            {
                return this.cyField;
            }
            set
            {
                this.cyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class prstGeom
    {

        private object avLstField;

        private string prstField;

        /// <remarks/>
        public object avLst
        {
            get
            {
                return this.avLstField;
            }
            set
            {
                this.avLstField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string prst
        {
            get
            {
                return this.prstField;
            }
            set
            {
                this.prstField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class ln
    {

        private lnTailEnd tailEndField;

        /// <remarks/>
        public lnTailEnd tailEnd
        {
            get
            {
                return this.tailEndField;
            }
            set
            {
                this.tailEndField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class lnTailEnd
    {

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeCxnSpStyle
    {

        private lnRef lnRefField;

        private fillRef fillRefField;

        private effectRef effectRefField;

        private fontRef fontRefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public lnRef lnRef
        {
            get
            {
                return this.lnRefField;
            }
            set
            {
                this.lnRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public fillRef fillRef
        {
            get
            {
                return this.fillRefField;
            }
            set
            {
                this.fillRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public effectRef effectRef
        {
            get
            {
                return this.effectRefField;
            }
            set
            {
                this.effectRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public fontRef fontRef
        {
            get
            {
                return this.fontRefField;
            }
            set
            {
                this.fontRefField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class lnRef
    {

        private lnRefSchemeClr schemeClrField;

        private byte idxField;

        /// <remarks/>
        public lnRefSchemeClr schemeClr
        {
            get
            {
                return this.schemeClrField;
            }
            set
            {
                this.schemeClrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte idx
        {
            get
            {
                return this.idxField;
            }
            set
            {
                this.idxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class lnRefSchemeClr
    {

        private lnRefSchemeClrShade shadeField;

        private string valField;

        /// <remarks/>
        public lnRefSchemeClrShade shade
        {
            get
            {
                return this.shadeField;
            }
            set
            {
                this.shadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string val
        {
            get
            {
                return this.valField;
            }
            set
            {
                this.valField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class lnRefSchemeClrShade
    {

        private ushort valField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort val
        {
            get
            {
                return this.valField;
            }
            set
            {
                this.valField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class fillRef
    {

        private fillRefSchemeClr schemeClrField;

        private byte idxField;

        /// <remarks/>
        public fillRefSchemeClr schemeClr
        {
            get
            {
                return this.schemeClrField;
            }
            set
            {
                this.schemeClrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte idx
        {
            get
            {
                return this.idxField;
            }
            set
            {
                this.idxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class fillRefSchemeClr
    {

        private string valField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string val
        {
            get
            {
                return this.valField;
            }
            set
            {
                this.valField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class effectRef
    {

        private effectRefSchemeClr schemeClrField;

        private byte idxField;

        /// <remarks/>
        public effectRefSchemeClr schemeClr
        {
            get
            {
                return this.schemeClrField;
            }
            set
            {
                this.schemeClrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte idx
        {
            get
            {
                return this.idxField;
            }
            set
            {
                this.idxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class effectRefSchemeClr
    {

        private string valField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string val
        {
            get
            {
                return this.valField;
            }
            set
            {
                this.valField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class fontRef
    {

        private fontRefSchemeClr schemeClrField;

        private string idxField;

        /// <remarks/>
        public fontRefSchemeClr schemeClr
        {
            get
            {
                return this.schemeClrField;
            }
            set
            {
                this.schemeClrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string idx
        {
            get
            {
                return this.idxField;
            }
            set
            {
                this.idxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class fontRefSchemeClr
    {

        private string valField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string val
        {
            get
            {
                return this.valField;
            }
            set
            {
                this.valField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeGrpSpPr
    {

        private xfrm xfrmField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public xfrm xfrm
        {
            get
            {
                return this.xfrmField;
            }
            set
            {
                this.xfrmField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeNvGrpSpPr
    {

        private sldCSldSpTreeNvGrpSpPrCNvPr cNvPrField;

        private object cNvGrpSpPrField;

        private object nvPrField;

        /// <remarks/>
        public sldCSldSpTreeNvGrpSpPrCNvPr cNvPr
        {
            get
            {
                return this.cNvPrField;
            }
            set
            {
                this.cNvPrField = value;
            }
        }

        /// <remarks/>
        public object cNvGrpSpPr
        {
            get
            {
                return this.cNvGrpSpPrField;
            }
            set
            {
                this.cNvGrpSpPrField = value;
            }
        }

        /// <remarks/>
        public object nvPr
        {
            get
            {
                return this.nvPrField;
            }
            set
            {
                this.nvPrField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeNvGrpSpPrCNvPr
    {

        private byte idField;

        private string nameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeSP
    {

        private sldCSldSpTreeSPNvSpPr nvSpPrField;

        private sldCSldSpTreeSPSpPr spPrField;

        private sldCSldSpTreeSPStyle styleField;

        private sldCSldSpTreeSPTxBody txBodyField;

        /// <remarks/>
        public sldCSldSpTreeSPNvSpPr nvSpPr
        {
            get
            {
                return this.nvSpPrField;
            }
            set
            {
                this.nvSpPrField = value;
            }
        }

        /// <remarks/>
        public sldCSldSpTreeSPSpPr spPr
        {
            get
            {
                return this.spPrField;
            }
            set
            {
                this.spPrField = value;
            }
        }

        /// <remarks/>
        public sldCSldSpTreeSPStyle style
        {
            get
            {
                return this.styleField;
            }
            set
            {
                this.styleField = value;
            }
        }

        /// <remarks/>
        public sldCSldSpTreeSPTxBody txBody
        {
            get
            {
                return this.txBodyField;
            }
            set
            {
                this.txBodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeSPNvSpPr
    {

        private sldCSldSpTreeSPNvSpPrCNvPr cNvPrField;

        private object cNvSpPrField;

        private object nvPrField;

        /// <remarks/>
        public sldCSldSpTreeSPNvSpPrCNvPr cNvPr
        {
            get
            {
                return this.cNvPrField;
            }
            set
            {
                this.cNvPrField = value;
            }
        }

        /// <remarks/>
        public object cNvSpPr
        {
            get
            {
                return this.cNvSpPrField;
            }
            set
            {
                this.cNvSpPrField = value;
            }
        }

        /// <remarks/>
        public object nvPr
        {
            get
            {
                return this.nvPrField;
            }
            set
            {
                this.nvPrField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeSPNvSpPrCNvPr
    {

        private extLst extLstField;

        private byte idField;

        private string nameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public extLst extLst
        {
            get
            {
                return this.extLstField;
            }
            set
            {
                this.extLstField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeSPSpPr
    {

        private xfrm xfrmField;

        private prstGeom prstGeomField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public xfrm xfrm
        {
            get
            {
                return this.xfrmField;
            }
            set
            {
                this.xfrmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public prstGeom prstGeom
        {
            get
            {
                return this.prstGeomField;
            }
            set
            {
                this.prstGeomField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeSPStyle
    {

        private lnRef lnRefField;

        private fillRef fillRefField;

        private effectRef effectRefField;

        private fontRef fontRefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public lnRef lnRef
        {
            get
            {
                return this.lnRefField;
            }
            set
            {
                this.lnRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public fillRef fillRef
        {
            get
            {
                return this.fillRefField;
            }
            set
            {
                this.fillRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public effectRef effectRef
        {
            get
            {
                return this.effectRefField;
            }
            set
            {
                this.effectRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public fontRef fontRef
        {
            get
            {
                return this.fontRefField;
            }
            set
            {
                this.fontRefField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldSpTreeSPTxBody
    {

        private bodyPr bodyPrField;

        private object lstStyleField;

        private p pField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public bodyPr bodyPr
        {
            get
            {
                return this.bodyPrField;
            }
            set
            {
                this.bodyPrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public object lstStyle
        {
            get
            {
                return this.lstStyleField;
            }
            set
            {
                this.lstStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public p p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class bodyPr
    {

        private byte rtlColField;

        private string anchorField;

        private string vertField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte rtlCol
        {
            get
            {
                return this.rtlColField;
            }
            set
            {
                this.rtlColField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string anchor
        {
            get
            {
                return this.anchorField;
            }
            set
            {
                this.anchorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string vert
        {
            get
            {
                return this.vertField;
            }
            set
            {
                this.vertField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", IsNullable = false)]
    public partial class p
    {

        private pPPr pPrField;

        private pR rField;

        private pEndParaRPr endParaRPrField;

        /// <remarks/>
        public pPPr pPr
        {
            get
            {
                return this.pPrField;
            }
            set
            {
                this.pPrField = value;
            }
        }

        /// <remarks/>
        public pR r
        {
            get
            {
                return this.rField;
            }
            set
            {
                this.rField = value;
            }
        }

        /// <remarks/>
        public pEndParaRPr endParaRPr
        {
            get
            {
                return this.endParaRPrField;
            }
            set
            {
                this.endParaRPrField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class pPPr
    {

        private string algnField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string algn
        {
            get
            {
                return this.algnField;
            }
            set
            {
                this.algnField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class pR
    {

        private pRRPr rPrField;

        private string tField;

        /// <remarks/>
        public pRRPr rPr
        {
            get
            {
                return this.rPrField;
            }
            set
            {
                this.rPrField = value;
            }
        }

        /// <remarks/>
        public string t
        {
            get
            {
                return this.tField;
            }
            set
            {
                this.tField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class pRRPr
    {

        private string langField;

        private ushort szField;

        private byte dirtyField;

        private byte errField;

        private bool errFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort sz
        {
            get
            {
                return this.szField;
            }
            set
            {
                this.szField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte dirty
        {
            get
            {
                return this.dirtyField;
            }
            set
            {
                this.dirtyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte err
        {
            get
            {
                return this.errField;
            }
            set
            {
                this.errField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool errSpecified
        {
            get
            {
                return this.errFieldSpecified;
            }
            set
            {
                this.errFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
    public partial class pEndParaRPr
    {

        private string langField;

        private ushort szField;

        private byte dirtyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort sz
        {
            get
            {
                return this.szField;
            }
            set
            {
                this.szField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte dirty
        {
            get
            {
                return this.dirtyField;
            }
            set
            {
                this.dirtyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldExtLst
    {

        private sldCSldExtLstExt extField;

        /// <remarks/>
        public sldCSldExtLstExt ext
        {
            get
            {
                return this.extField;
            }
            set
            {
                this.extField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldCSldExtLstExt
    {

        private creationId1 creationIdField;

        private string uriField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.microsoft.com/office/powerpoint/2010/main")]
        public creationId1 creationId
        {
            get
            {
                return this.creationIdField;
            }
            set
            {
                this.creationIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uri
        {
            get
            {
                return this.uriField;
            }
            set
            {
                this.uriField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.microsoft.com/office/powerpoint/2010/main")]
    [System.Xml.Serialization.XmlRootAttribute("creationId", Namespace = "http://schemas.microsoft.com/office/powerpoint/2010/main", IsNullable = false)]
    public partial class creationId1
    {

        private long valField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long val
        {
            get
            {
                return this.valField;
            }
            set
            {
                this.valField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.openxmlformats.org/presentationml/2006/main")]
    public partial class sldClrMapOvr
    {

        private object masterClrMappingField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.openxmlformats.org/drawingml/2006/main")]
        public object masterClrMapping
        {
            get
            {
                return this.masterClrMappingField;
            }
            set
            {
                this.masterClrMappingField = value;
            }
        }
    }


}
