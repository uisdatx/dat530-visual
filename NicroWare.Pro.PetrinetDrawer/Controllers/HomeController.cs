﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using NicroWare.Lib.MatLabGen;
using NicroWare.Pro.PetrinetDrawer.Models;

namespace NicroWare.Pro.PetrinetDrawer.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Open()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save()
        {
            return View();
        }

        [HttpGet]
        [Route("/customaction")]
        public IActionResult CustomAction()
        {
            Process p = Process.Start("custom.bat");
            p.WaitForExit();
            return View();
        }

        [HttpPost]
        [Route("/save")]
        public async Task<IActionResult> OtherSave(string filename, string xml)
        {
            string newData = Uri.UnescapeDataString(xml);

            System.IO.File.WriteAllText(filename, newData);
            
            return Ok();
        }

        [HttpPost]
        [Route("/savepetri")]
        public IActionResult PetrinetSave(string filename, string xml)
        {
            string newData = Uri.UnescapeDataString(xml);

            System.IO.File.WriteAllText(filename, newData);

            XmlSerializer serializer = new XmlSerializer(typeof(mxGraphModel));
            mxGraphModel model = serializer.Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(newData))) as mxGraphModel;

            List<string> errors = new List<string>();

            Petrinet net = new Petrinet();
            foreach (var node in model.root.Where(x => x.style != null && x.parent == 1 && !x.style.StartsWith("text")))
            {
                //TODO: Fix derived objects that are linked, if parrent is not equal to 1, then it should be linked to another object
                if (node.edge > 0)
                {
                    int connections = 1;
                    if (!string.IsNullOrWhiteSpace(node.value))
                    {
                        if (int.TryParse(node.value, out int newCons))
                        {
                            connections = newCons;
                        }
                        else
                        {
                            errors.Add($"Could not parse value of Connection: {node.value}");
                        }
                    }
                    net.AddConnection(new Connection() { ID = node.id, Name = "Connection_" + node.id, StartIndex = node.source, EndIndex = node.target, Count = connections });
                }
                else
                {
                    string nodeValue = StripHTML(node.value);
                    if (node.style.Contains("ellipse"))
                    {
                        net.AddShape(new ShapeInfo() { ID = node.id, Text = nodeValue, Name = "Place_" + node.id, Geometry = ShapeType.Place });
                    }
                    else
                    {
                        net.AddShape(new ShapeInfo() { ID = node.id, Text = nodeValue, Name = "Transition_" + node.id, Geometry = ShapeType.Transition });
                    }
                }
            }
            string modelName = filename.Replace(".xml", "");

            errors.AddRange(net.VerifyPetrinet());

            if (errors.Count > 0)
            {
                return BadRequest(new { message = "\n" + string.Join("\n", errors) });
            }

            net.GeneratePetrinet(modelName + "_pdf.m", modelName);
            return Ok();
        }

        private static string StripHTML(string data)
        {
            Regex regex = new Regex("<[^>]+>");
            return regex.Replace(data, "");
        }

        [HttpPost]
        [Route("/export")]
        public IActionResult ExportStuff()
        {
            StreamReader sr = new StreamReader(Request.Body);
            string s = sr.ReadToEnd();
            return View("save");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
