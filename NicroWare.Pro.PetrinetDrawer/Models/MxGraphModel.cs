﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NicroWare.Pro.PetrinetDrawer.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class mxGraphModel
    {

        private mxGraphModelMxCell[] rootField;

        private int dxField;

        private int dyField;

        private int gridField;

        private int gridSizeField;

        private int guidesField;

        private int tooltipsField;

        private int connectField;

        private int arrowsField;

        private int foldField;

        private int pageField;

        private int pageScaleField;

        private int pageWidthField;

        private int pageHeightField;

        private string backgroundField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("mxCell", IsNullable = false)]
        public mxGraphModelMxCell[] root
        {
            get
            {
                return this.rootField;
            }
            set
            {
                this.rootField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int dx
        {
            get
            {
                return this.dxField;
            }
            set
            {
                this.dxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int dy
        {
            get
            {
                return this.dyField;
            }
            set
            {
                this.dyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int grid
        {
            get
            {
                return this.gridField;
            }
            set
            {
                this.gridField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int gridSize
        {
            get
            {
                return this.gridSizeField;
            }
            set
            {
                this.gridSizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int guides
        {
            get
            {
                return this.guidesField;
            }
            set
            {
                this.guidesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int tooltips
        {
            get
            {
                return this.tooltipsField;
            }
            set
            {
                this.tooltipsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int connect
        {
            get
            {
                return this.connectField;
            }
            set
            {
                this.connectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int arrows
        {
            get
            {
                return this.arrowsField;
            }
            set
            {
                this.arrowsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int fold
        {
            get
            {
                return this.foldField;
            }
            set
            {
                this.foldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int page
        {
            get
            {
                return this.pageField;
            }
            set
            {
                this.pageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int pageScale
        {
            get
            {
                return this.pageScaleField;
            }
            set
            {
                this.pageScaleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int pageWidth
        {
            get
            {
                return this.pageWidthField;
            }
            set
            {
                this.pageWidthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int pageHeight
        {
            get
            {
                return this.pageHeightField;
            }
            set
            {
                this.pageHeightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string background
        {
            get
            {
                return this.backgroundField;
            }
            set
            {
                this.backgroundField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class mxGraphModelMxCell
    {

        private mxGraphModelMxCellMxGeometry mxGeometryField;

        private int idField;

        private int parentField;

        private bool parentFieldSpecified;

        private string valueField;

        private string styleField;

        private int edgeField;

        private bool edgeFieldSpecified;

        private int sourceField;

        private bool sourceFieldSpecified;

        private int targetField;

        private bool targetFieldSpecified;

        private int vertexField;

        private bool vertexFieldSpecified;

        /// <remarks/>
        public mxGraphModelMxCellMxGeometry mxGeometry
        {
            get
            {
                return this.mxGeometryField;
            }
            set
            {
                this.mxGeometryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int parent
        {
            get
            {
                return this.parentField;
            }
            set
            {
                this.parentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool parentSpecified
        {
            get
            {
                return this.parentFieldSpecified;
            }
            set
            {
                this.parentFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string style
        {
            get
            {
                return this.styleField;
            }
            set
            {
                this.styleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int edge
        {
            get
            {
                return this.edgeField;
            }
            set
            {
                this.edgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool edgeSpecified
        {
            get
            {
                return this.edgeFieldSpecified;
            }
            set
            {
                this.edgeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool sourceSpecified
        {
            get
            {
                return this.sourceFieldSpecified;
            }
            set
            {
                this.sourceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool targetSpecified
        {
            get
            {
                return this.targetFieldSpecified;
            }
            set
            {
                this.targetFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int vertex
        {
            get
            {
                return this.vertexField;
            }
            set
            {
                this.vertexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool vertexSpecified
        {
            get
            {
                return this.vertexFieldSpecified;
            }
            set
            {
                this.vertexFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class mxGraphModelMxCellMxGeometry
    {
        private mxGraphModelMxCellMxGeometryMxPoint mxPointField;

        private int relativeField;

        private bool relativeFieldSpecified;

        private string asField;

        private double xField;

        private bool xFieldSpecified;

        private double yField;

        private bool yFieldSpecified;

        private double widthField;

        private bool widthFieldSpecified;

        private double heightField;

        private bool heightFieldSpecified;

        /// <remarks/>
        public mxGraphModelMxCellMxGeometryMxPoint mxPoint
        {
            get
            {
                return this.mxPointField;
            }
            set
            {
                this.mxPointField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int relative
        {
            get
            {
                return this.relativeField;
            }
            set
            {
                this.relativeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool relativeSpecified
        {
            get
            {
                return this.relativeFieldSpecified;
            }
            set
            {
                this.relativeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string @as
        {
            get
            {
                return this.asField;
            }
            set
            {
                this.asField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public double x
        {
            get
            {
                return this.xField;
            }
            set
            {
                this.xField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool xSpecified
        {
            get
            {
                return this.xFieldSpecified;
            }
            set
            {
                this.xFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public double y
        {
            get
            {
                return this.yField;
            }
            set
            {
                this.yField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ySpecified
        {
            get
            {
                return this.yFieldSpecified;
            }
            set
            {
                this.yFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public double width
        {
            get
            {
                return this.widthField;
            }
            set
            {
                this.widthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool widthSpecified
        {
            get
            {
                return this.widthFieldSpecified;
            }
            set
            {
                this.widthFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public double height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool heightSpecified
        {
            get
            {
                return this.heightFieldSpecified;
            }
            set
            {
                this.heightFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class mxGraphModelMxCellMxGeometryMxPoint
    {

        private string asField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string @as
        {
            get
            {
                return this.asField;
            }
            set
            {
                this.asField = value;
            }
        }
    }

}
