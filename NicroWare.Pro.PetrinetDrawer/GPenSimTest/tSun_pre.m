function [fire, transition] = tSun_pre(transition)
global PN;

% (sin(2*pi*(PN.current_time-0.25)) + ( (sin(2*pi*(PN.current_time-82)/365.2422)+1)/2*(18.5-6.18)/12 + 6.18/12 - 1) * sqrt(2))/2;

% fire = sin(2*pi*(PN.current_time)/48) >= 0;
fire = (sin(2*pi*(PN.current_time-0.25)) + ( (sin(2*pi*(PN.current_time-82)/365.2422)+1)/2*(18.5-6.18)/12 + 6.18/12 - 1) * sqrt(2))/2 >= 0;


% In transition
%               name: 'tSun'
%                tid: 2
%          new_color: {}
%           override: 0
%    selected_tokens: []
%    additional_cost: 0